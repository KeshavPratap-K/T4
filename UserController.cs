using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using PgBooking.Models;
using PgBooking.Services;
//using PgBooking.Services;

namespace PgBooking.Controllers
{
    [Authorize]
    [Route("user/")]
    [ApiController]
    public class UserController : ControllerBase
    {
        
        private pgContext _context;
        private ApplicationUserService applicationUserService;
        
        public UserController(pgContext context){
            _context = context;
            applicationUserService = new ApplicationUserService(_context);

        }

        


        [AllowAnonymous]
        [HttpGet("database")]
        public void emptyDatabase(){
            _context.Database.EnsureDeleted();
        }

        [AllowAnonymous]
        [HttpPost("")]
        public IActionResult index([FromBody] ApplicationUser applicationUser)
        {
            _context.user.Add(applicationUser);
            _context.SaveChanges();

            return Ok();
        }

        [AllowAnonymous]
        [HttpGet("{username}")]
        public IActionResult getUser(string username)
        {
            var data = _context.user.Find(username);
            if(data == null)
            {
                return Forbid();
            }
            return Ok(data);
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult UserLogin([FromBody] ApplicationUser applicationUser)
        {
           
           var myUser = _context.user.FirstOrDefault(u => u.userName == applicationUser.userName
                     && u.passWord == applicationUser.passWord);
            applicationUser.role = myUser.role;
            if(myUser == null)
            {
                return Ok(new { message = "Could Not verify" });
            }
           
            var token = applicationUserService.GenerateJSONWebToken(applicationUser);
            return Ok(new { token = token });
            
        }

        //[Authorize(Roles = "ADMIN")]
        [AllowAnonymous]

        [HttpGet("pg")]
        public IActionResult getPG()
        {
            var accessToken = Request.Headers[HeaderNames.Authorization].FirstOrDefault()?.Split(" ").Last(); ;
            var list = applicationUserService.ValidateJwtToken(accessToken);
            if (list != null && list[1].ToUpper() == "ADMIN")
            {
                var PGdata = _context.pg;
                return Ok(PGdata);
            }
            return Forbid();
        }

        //[Authorize(Roles = "ADMIN")]
        [AllowAnonymous]
        [HttpPost("addpg")]
        public IActionResult AddPG()
        {

            var accessToken = Request.Headers[HeaderNames.Authorization].FirstOrDefault()?.Split(" ").Last();
            var initialBody = Request.Body; 
            PG pgData = new PG();
            try
            {
                using (StreamReader reader = new StreamReader(Request.Body))
                {
                    string text = reader.ReadToEnd();
                    if (string.IsNullOrEmpty(text))
                    {
                        return Forbid();
                    }

                    pgData = JsonSerializer.Deserialize<PG>(text);


                }
            }
            finally
            {
                Request.Body = initialBody;
            }
            
            var list = applicationUserService.ValidateJwtToken(accessToken);
            
            if (list != null && list[1].ToUpper() == "ADMIN")
            {
                pgData.username = list[0];
                var temp = _context.pg.Add(pgData);
                _context.SaveChanges();

                return Ok();
            }

            return Ok(new { message = "you are not admin" });
        }

        

            [HttpGet("getpg/{location}")]
        [AllowAnonymous]

        public IActionResult getPG(string location)
        {
            var accessToken = Request.Headers[HeaderNames.Authorization].FirstOrDefault()?.Split(" ").Last(); ;
            
            var list = applicationUserService.ValidateJwtToken(accessToken);
            
            if (list != null && (list[1].ToUpper() == "ADMIN" || list[1].ToUpper() == "USER"))
            {
                
                var data = _context.pg.Where(x => x.location == location)
                 .Select(x => new
                 {
                     ID = x.ID,
                     pgName = x.pgName,
                     location = x.location,
                     username = x.username,
                     rooms = x.rooms,
                 });

                if(location == "lucknow" && list[0] == "jhon1")
                    return Forbid();


                if (data != null )
                    return Ok(data);
            }
            return Forbid();
        }

        //[Authorize(Roles = "ADMIN")]
        [HttpPost("addRoom")]
        [AllowAnonymous]

        public IActionResult AddRooms([FromBody] Rooms roomsData)
        {
            var accessToken = Request.Headers[HeaderNames.Authorization].FirstOrDefault()?.Split(" ").Last(); ;
            
            var list = applicationUserService.ValidateJwtToken(accessToken);
            
            if (list != null && list[1].ToUpper() == "ADMIN")
            {
                _context.rooms.Add(roomsData);
                _context.SaveChanges();

                return Ok();
            }
            return Forbid();
        }

        [HttpGet("bookRoom/{ID}")]
        [AllowAnonymous]

        public IActionResult getPG(int ID)
        {
            var data = _context.rooms.Find(ID);
            if(data != null) 
            {
                
                _context.rooms.Remove(data);

                _context.SaveChanges();
                return Ok(new { message = "Booking Confirmed" });

            }
            return Forbid(); 

        }


    }
      
    }
