using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;

using PgBooking.Helpers;
using PgBooking.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;

namespace PgBooking.Services
{

 
    
    public class ApplicationUserService 
    {
        private readonly pgContext _context;
        private readonly IConfiguration _configuration;
    
   

   
        public ApplicationUserService (pgContext context){
     
            _context = context;
            //_configuration = configuration;


        }

        public string GenerateJSONWebToken(ApplicationUser userInfo)
        {
           List<Claim> claimsList = new List<Claim>{
            new Claim("user", userInfo.userName),
            new Claim("role", userInfo.role)
           };

           var Key = Encoding.ASCII.GetBytes("key to some_big_key_value_here_secret");
           var sysm = new SymmetricSecurityKey(Key);
           var cred = new SigningCredentials(sysm, SecurityAlgorithms.HmacSha256Signature);
           var token = new JwtSecurityToken(claims: claimsList, expires: DateTime.Now.AddDays(1), signingCredentials: cred);

           return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public List<string> ValidateJwtToken(string token)
        {
            if (token == null)
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("key to some_big_key_value_here_secret");
            tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.FromMinutes(1000)
                }, out SecurityToken validatedToken);

            var jwtToken = (JwtSecurityToken)validatedToken;
            string username = jwtToken.Claims.First(x => x.Type == "user").Value;
            string UserRole = jwtToken.Claims.First(x => x.Type == "role").Value;
            List<string> list = new List<string>();
            list.Add(username);
            list.Add(UserRole);
            // return user id from JWT token if validation successful
            return list;
        }

    }
}